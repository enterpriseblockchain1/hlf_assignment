pwd =>  /ws/hlf_assignment/fabric-samples/test-network

<!-- #Set the path for peer binary and config for core.yaml -->
export PATH=/ws/hlf_assignment/fabric-samples/test-network/../bin:$PATH
export FABRIC_CFG_PATH=/ws/hlf_assignment/fabric-samples/test-network/../config/

#Set the environment variables to operate Peer as Org1
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_MSPCONFIGPATH=/ws/hlf_assignment/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_TLS_ROOTCERT_FILE=/ws/hlf_assignment/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt

<!-- interacting with the network -->
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride
orderer.example.com --tls --cafile
"/ws/hlf_assignment/fabric-samples/test-network/organizations/ordererOrganizations/example.com/orderers/orderer.exa
mple.com/msp/tlscacerts/tlsca.example.com-cert.pem" -C mychannel -n basic
--peerAddresses localhost:7051 --tlsRootCertFiles
"/ws/hlf_assignment/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.e
xample.com/tls/ca.crt" --peerAddresses localhost:9051 --tlsRootCertFiles
"/ws/hlf_assignment/fabric-samples/test-network/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.e
xample.com/tls/ca.crt" -c '{"function":"InitLedger","Args":[]}'